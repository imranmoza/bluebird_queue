package main

import (
	"bb_go/controller"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("Successfully connected!")
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/api/getqueue/{parameter}/", controller.GetQueue).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/sendmessage/", controller.SendMessage).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/getlistqueue/{parameter}/", controller.GetListQueue).Methods("GET", "OPTIONS") //GetListQueue
	http.ListenAndServe(":8000", router)
}
