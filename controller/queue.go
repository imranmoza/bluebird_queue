package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type data_queue struct {
	Detail []queue_prsn `json:"detail"`
}

type queue_prsn struct {
	QueueId     string `json:"queue_id"`
	QueueName   string `json:"queue_name"`
	QueueNumber string `json:"queue_number"`
	Message     string `json:"message"`
}

var vque int
var data data_queue
var vdtjsn string

func GetListQueue(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, string(vdtjsn))
}

func GetQueue(w http.ResponseWriter, r *http.Request) {
	Parameter := mux.Vars(r)["parameter"]

	vque = vque + 1

	emp := &queue_prsn{QueueId: strconv.Itoa(vque), QueueName: string(Parameter), QueueNumber: strconv.Itoa(vque)}
	e, err := json.Marshal(emp)
	if err != nil {
		fmt.Println(err)
		return
	}

	GetDataQueue(strconv.Itoa(vque), string(Parameter), "strconv.Itoa(vque)")
	//fmt.Println("Successfully connected!")
	fmt.Fprintf(w, string(e))
}

func GetDataQueue(id string, name string, message string) {

	data = data_queue{
		Detail: []queue_prsn{
			queue_prsn{
				QueueId:     id,
				QueueName:   name,
				QueueNumber: id,
				Message:     message},
		},
	}
	dtjsn, _ := json.Marshal(data)
	vdtjsn = string(dtjsn)
	fmt.Println(string(dtjsn))
}

func SendMessage(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)

	var format queue_prsn
	json.Unmarshal([]byte(body), &format)

	data = data_queue{
		Detail: []queue_prsn{
			queue_prsn{
				QueueId:     string(format.QueueId),
				QueueName:   string(format.QueueName),
				QueueNumber: string(format.QueueId),
				Message:     string(format.Message)},
		},
	}
	dtjsn, _ := json.Marshal(data)
	vdtjsn = string(dtjsn)
	fmt.Println(string(dtjsn))

	//fmt.Println("Successfully connected!")
	fmt.Fprintf(w, string("{\"status\":\"Message sent !\"}"))
}
