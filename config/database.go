package config

import(
	"database/sql"
    "fmt"
    "strconv"

    _ "github.com/mattn/go-sqlite3"
)

var DB sql*DB;

func ConnectDB() error {
	var err error
	
	DB, _ := sql.Open("sqlite3", "./imran.db")
	
    statement, _ := DB.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	statement.Exec()

	fmt.Printf("Connected to %s database\n")
	return nil
}